#!/bin/bash

version=$(cat app.py | grep 'version =' | cut -d "b" -f 2 | tr -d "'")
git_sha=$(git show-ref | cut -d " " -f 1)
docker build -t "nibalizer/testapp" \
             -t "nibalizer/testapp:${version}" .
#             -t "nibalizer/testapp:${git_sha}" 

docker push nibalizer/testapp
