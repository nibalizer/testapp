FROM jfloff/alpine-python:3.6-onbuild

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ADD app.py .
ADD requirements.txt .
RUN pip install -r requirements.txt

# for a flask server
EXPOSE 8080
CMD python app.py
