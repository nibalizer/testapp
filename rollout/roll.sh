#!/bin/bash
echo $0

function canary_on () {

temp_istio=$(mktemp)
cat VirtualService.yaml | CANARYWEIGHT=20 PRODWEIGHT=80 envsubst >> $temp_istio
echo "Turning on canary service at 5%"
kubectl apply -f $temp_istio
istioctl get virtualservice -o yaml
echo $temp_istio
#rm $temp_istio

}

function canary_off () {

temp_istio=$(mktemp)
cat VirtualService.yaml | CANARYWEIGHT=0 PRODWEIGHT=100 envsubst >> $temp_istio
echo "Turning off canary service entirely"
kubectl apply -f $temp_istio
istioctl get virtualservice -o yaml
echo $temp_istio

}


function deployment_gen () {
VERSION_NUMBER=$1
VERSION=$2

cat deploy.yaml | VERSION=$VERSION VERSION_NUMBER=$VERSION_NUMBER envsubst

}

if [ $0 = './create_canary.sh' ]; then
  VERSION_NUMBER=$1
  
  istioctl kube-inject -f <(deployment_gen $VERSION_NUMBER canary) | kubectl apply -f -
  kubectl wait deployment --timeout=60s --for condition=available -l version=canary
  canary_on 

fi

if [ $0 = "./promote_canary.sh" ]; then
  VERSION_NUMBER=$1

  istioctl kube-inject -f <(deployment_gen $VERSION_NUMBER prod) | kubectl apply -f -
  kubectl wait deployment --timeout=60s --for condition=available -l version=prod
#  kubectl delete pod -l app=testapp
  canary_off
  kubectl delete deploy/testapp-canary-deployment

fi




